import java.util.Date;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.io.*;
import java.net.URL;

public class PostFile{
    public static int MAX_CHUNK = 10 * 1024 * 1024;

    public static String main(BufferedInputStream remote, String filename, String url, String token, String transformation) throws IOException {
         
        String boundary = "browndog-fence-header";
        
 	    URLConnection connection = new URL(url).openConnection();
         
        // enable streaming mode, max chunk to hold in memory is 10MB
        ((HttpURLConnection) connection).setChunkedStreamingMode(MAX_CHUNK);
            
        connection.setRequestProperty("Authorization",token);
         if(transformation.equals("convert")){
             connection.setRequestProperty("Content-Type","multipart/form-data; boundary="+boundary);
         } else if(transformation.equals("extract")) {
            connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded; boundary="+boundary);
         }
         
        connection.setDoOutput(true);
        connection.setDoInput(true);

        // post data
        OutputStream output = connection.getOutputStream();

        output.write(("--" + boundary + "\r\n").getBytes());
        output.write(("Content-Disposition: form-data; name=\"File\"; filename=\"" + filename + "\"\r\n").getBytes());
        output.write(("Content-Type: image/jpeg\r\n\r\n").getBytes());

        byte[] buf = new byte[1024 * 1024];
        int len;
        while ((len = remote.read(buf)) > 0) {
            output.write(buf, 0, len);
        }
        remote.close();

        output.write(("\r\n--" + boundary + "--\r\n").getBytes());
        output.flush();
        output.close();

        // read result
        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      
        HttpURLConnection httpConn = (HttpURLConnection) connection;
        int statusCode = httpConn.getResponseCode(); 
 
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
